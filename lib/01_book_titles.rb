class Book
  LOWER_WORDS = %w(the a an and in of)

  attr_reader :title

  def title=(title)
    @title = title.downcase.split.map.with_index do |word, i|
      if LOWER_WORDS.include?(word) && i != 0
        word
      else
        word.capitalize
      end
    end.join(" ")
  end
end
