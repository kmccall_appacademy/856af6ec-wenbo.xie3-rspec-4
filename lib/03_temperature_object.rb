class Temperature
  def initialize(options)
    if options[:f]
      @temperature = options[:f]
      @scale = :f
    else
      @temperature = options[:c]
      @scale = :c
    end
  end

  def self.ctof(temp)
    temp * 9.fdiv(5) + 32
  end

  def self.ftoc(temp)
    (temp - 32) * 5.fdiv(9)
  end

  def in_fahrenheit
    if @scale == :f
      @temperature
    else
      self.class.ctof(@temperature)
    end
  end

  def in_celsius
    if @scale == :c
      @temperature
    else
      self.class.ftoc(@temperature)
    end
  end

  def self.from_fahrenheit(temp)
    self.new(f: temp)
  end

  def self.from_celsius(temp)
    self.new(c: temp)
  end
end

class Celsius < Temperature
  def initialize(temp)
    @temperature = temp
    @scale = :c
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    @temperature = temp
    @scale = :f
  end
end
