class Dictionary
  attr_reader :entries

  def initialize
    @entries = {}
  end

  def add(vocab)
    if vocab.is_a?(String)
      entries[vocab] = nil
    else
      entries.merge!(vocab)
    end
  end

  def keywords
    entries.keys.sort
  end

  def include?(word)
    entries.has_key?(word)
  end

  def find(chars)
    entries.select do |word, definition|
      word.include?(chars)
    end
  end

  def printable
    keywords.map do |keyword|
      %Q{[#{keyword}] "#{entries[keyword]}"}
    end.join("\n")
  end
end
